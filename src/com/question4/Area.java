package com.question4;

public class Area extends Shape {
	public static void main(String[] args) {
		
		Area a=new Area();
		a.rectangle_area(5, 23);
		a.square_area(3);
		a.circle_area(15);
	}
	@Override
	public void rectangle_area(int a, int b) {
		 System.out.println("the area of the rectangle is "+a*b+" sq units");
		
	}

	@Override
	public void square_area(int c) {
		
		System.out.println("the area of the square is "+Math.pow(c, 2)+" sq units");
		
	}

	@Override
	public void circle_area(int r) {
		 double z = 3.14 * r * r;
	        System.out.println("the area of the circle is "+z+" sq units");
		
	}
	
}
