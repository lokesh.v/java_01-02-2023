package com.questioin1;

public class Notebook extends Book{
	public static void main(String[] args) {
		System.out.println("Method used in notebook are :");
       Notebook n= new Notebook();
       n.write();
       n.read();
       n.draw();       
	  System.out.println("write and read are abstract method from class Book"); 
	}
@Override
public void write() {
   System.out.println("write");
}

@Override
public void read() {
	System.out.println("read");
	
}
public void draw() {
	System.out.println("draw");
}
}
