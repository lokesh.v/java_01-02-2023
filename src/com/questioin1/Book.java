package com.questioin1;

public abstract class Book {

	public abstract void write() ;
	public abstract void read();
}
