package com.question2;

public abstract class Telephone {
	public abstract void lift();
	public abstract void disconnect();
}
