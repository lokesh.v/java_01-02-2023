package com.question2;

public class Occurance {
	 public static void main(String[] args) {

	        String word = "Hello, World";

	        char c = 'o';

	        int index1 = word.indexOf(c);
	        System.out.println("First occurrence of character 'o' is at the index of :" + index1);

	        int index2 = word.lastIndexOf(c);
	        System.out.println("Last occurrence of character 'o' is at the index of :" + index2);
          
	        char c1 = ',';

	        int index = word.indexOf(c1);
	        System.out.println("occurrence of character ',' is at the index of :" + index);

	        
	        
	    }
}
