package com.question3;

public class TV implements Smart_TVremote{
	public static void main(String[] args) {
		TV t= new TV();
		t.tv();
		t.smartTvRemote();
		t.tvRemote();
	
	}
	
	public void tv() {
		System.out.println("This TV has access to :");
	}

	@Override
	public void tvRemote() {
		// TODO Auto-generated method stub
		System.out.println("Normal TV remote");
	}

	@Override
	public void smartTvRemote() {
		// TODO Auto-generated method stub
		System.out.println("Smart TV remote");
	}

}
