package com.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Array_List {

	public static void main(String[] args) {
		ArrayList<String> arr=new ArrayList<String>();
		arr.add("Lays");
		arr.add("Milk");
		arr.add("panner");
		arr.add("biscuits");
		arr.add("coke");
	
		System.out.println("Product in the list is :"+arr);
		System.out.println("Size of the List is :"+arr.size());
	    arr.remove(1);
	    System.out.println("2nd product removed from the List and now new list is :"+arr);
	    
	    if(arr.contains("coke")) {
	    	System.out.println("coke is present in the Array_List arr");
	    }
	    else{
	    	System.out.println("coke is not in the Array_List arr");
	    }
	}
}
